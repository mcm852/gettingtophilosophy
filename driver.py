import requests
import sys

url = 'http://localhost:8080/find?startingUrl=' +sys.argv[1]

response = requests.get(url)
data = response.json()

links = data['content']
links = links.strip('[')
links = links.strip(']')
links = links.split(', ')

num_hops = data['hops']

for l in links:
	print l

print ''
print 'Total of ', num_hops, 'hops'

