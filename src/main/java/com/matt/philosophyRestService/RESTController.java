package com.matt.philosophyRestService;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.cj.jdbc.MysqlDataSource;

@RestController
public class RESTController {
    private final AtomicLong counter = new AtomicLong();
    private WikiCrawler crawler = new WikiCrawler();

    @RequestMapping("/find")
    public Response greeting(@RequestParam(value="startingUrl", required=true) String startingUrl) {
    	
    	List<String> urls = crawler.findPhilosophy(startingUrl);
    	
    	if(null != urls) {
    		System.out.println(urls.hashCode());
        	System.out.println(urls.toString());
        	
        	writeToDB(urls);
        	
            return new Response(counter.incrementAndGet(), urls.toString(), urls.size());
    	}
    	else {
    		return new Response(counter.incrementAndGet(), "Could not find Philosophy", 0);
    	}
    	
    	
    }
    
    private void writeToDB(List<String> urls) {
    	MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setUser("wikiurls");
		dataSource.setPassword("Yd08WQ?_1xvG");
		dataSource.setServerName("mysql1.gear.host");
		dataSource.setDatabaseName("wikiurls");
		
    	Connection conn;
    	try {
    		conn = dataSource.getConnection();
			Statement stmt = conn.createStatement();
			String sql = "INSERT INTO paths (hashcode,path) VALUES (\"" + urls.hashCode() + "\",\"" + urls.toString() + "\")";
			stmt.executeUpdate(sql);
			
    	}
    	catch (SQLException e) {
			System.out.println("Path already exists in DB");
		}
    	
    	
    }

}
