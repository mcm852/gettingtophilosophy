package com.matt.philosophyRestService;

public class Response {
	
	private final long id;
    private final String content;
    private final int hops;

    public Response(long id, String content, int hops) {
        this.id = id;
        this.content = content;
        this.hops = hops;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
    
    public int getHops() {
    	return hops;
    }

}
