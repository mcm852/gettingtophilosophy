package com.matt.philosophyRestService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WikiCrawler {

	List<String> findPhilosophy(String url) {

		List<String> linksVisited = new ArrayList<String>();
		linksVisited.add(url);
		
		while (!"http://en.wikipedia.org/wiki/Philosophy".equalsIgnoreCase(url)) {
			
			// Assuming no path is greater than this
			if(linksVisited.size() > 200) {
				return null;
			}
			
			Document doc = null;
			try {
				doc = Jsoup.connect(url).get();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Element articleBody = doc.getElementById("mw-content-text");
			Elements pElements = articleBody.select("p");

			
			StringBuilder paragraphs = new StringBuilder();
			for (Element e : pElements) {
				paragraphs.append(e.toString());
			}


			doc = Jsoup.parse(removeParens(paragraphs.toString()));
			Elements links = doc.select("a");

			for (Element e : links) {
				// System.out.println(e.toString());
				String linkFromPage = e.toString().substring(9, e.toString().indexOf("\"", 10));

				if (!linkFromPage.contains("Help:") && !linksVisited.contains("http://en.wikipedia.org" + linkFromPage)
						&& linkFromPage.contains("/wiki/")) {
					url = "http://en.wikipedia.org" + linkFromPage;
					linksVisited.add(url);
					System.out.println(url);
					break;
				}
			}
		}

		return linksVisited;
	}

	static String removeParens(String p) {
		StringBuilder res = new StringBuilder();
		Stack<Character> parenStack = new Stack<Character>();
		boolean inLink = false;

		for (int i = 0; i < p.length(); ++i) {

			char curr = p.charAt(i);

			if (p.charAt(i) == '<' && p.charAt(i + 1) == 'a') {
				inLink = true;
				// System.out.println("inLink");
			} else if (p.charAt(i) == '/' && p.charAt(i + 1) == 'a') {
				inLink = false;
				// System.out.println("out of link");
			}

			if (p.charAt(i) == '(' && !inLink) {
				parenStack.push(curr);
			}

			else if (parenStack.isEmpty()) {
				res.append(curr);
			}

			else if (p.charAt(i) == ')') {
				parenStack.pop();
			}

		}

		return res.toString();
	}

}
