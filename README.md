# README #

This repository contains all source code for the service and driver, as well as the runnable JAR for the service.

To start the service:
java -jar target/philosophyRestService-1.0.0-SNAPSHOT.jar

The service will begin running on localhost:8080

To run the front-end driver:
python driver.py [starting url of wikipedia page]

Note that to run the driver, you will need
Python 2.7.14, and
The Python Requests module. If you do not have the requests module, you can install it with 'pip install requests'

Alternatively, query the web service by opening your browser an navigating to http://localhost:8080/find?startingUrl=yourWikiURLHere
With "yourWikiURLHere" being the URL of the Wikipedia page you want to start with. The result will be returned as raw JSON
